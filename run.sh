#!/bin/bash

URL=test
PORT=3030
NET=testforcandidates_mt_dev
IP=192.168.97.100
DIR="$(cd "$(dirname "$z")"; pwd)/$(basename "$z")src"

if [ $# -eq 0 ];
    then
        echo ""
        echo "No command entered:"
        echo "  -start            start docker"
        echo "  -restart          restart docker"
        echo "  -login            login $URL container"
        echo "  -reboot           reboot nginx"
        echo "  -stop             stop docker"
        echo "  -stop-all         stop all running docker containers"

elif [ $1 = '-test' ];
    then
        echo "### testing bash"
        echo $DIR

elif [ $1 = "-start" ];
    then
        echo "# starting docker image $URL"
		docker-compose up -d
        docker build -t $URL .
        docker run -d -p $PORT:80 -v $DIR:/var/www --net $NET --ip $IP --name $URL $URL

elif [ $1 = "-restart" ];
    then
        echo "# restarting docker image $URL"
        docker stop $URL
        docker rm $URL
		docker-compose down
		docker-compose up -d
        docker build -t $URL .
        docker run -d -p $PORT:80 -v $DIR:/var/www --net $NET --ip $IP $URL --name $URL


elif [ $1 = "-login" ];
    then
        echo "# login $URL container"
        docker exec -it $URL bash

elif [ $1 = "-reboot" ];
    then
        echo "# rebooting nginx"
        docker exec -ti $URL sh -c "/etc/init.d/nginx restart"

elif [ $1 = "-stop" ];
    then
        echo "# stopping docker image $URL"
        docker stop $URL
        docker rm $URL
		docker-compose down

elif [ $1 = "-stop-all" ];
    then
        echo "# stopping all active docker containers"
        docker stop $(docker ps -a -q)
        docker rm $(docker ps -a -q)

fi

# docker-machine rm -f default
