<?php

class Numbers
{
    public function roundToHundreds($min = 0, $max = 10000) {
        $range = $max - $min;
        $num = $min + $range * (mt_rand() / mt_getrandmax());
        $num = round($num, 2);

        return number_format($num,2,".","");
    }
}