<?php

class MailGenerator
{
    function mail($name,$surname) {

        $mailServers = ['gmail.com','yahoo.com','yandex.ru','mail.lt','inbox.lt','test.com','company.eu'];
        $randomServer = $mailServers[rand(0,count($mailServers)-1)];

        $name = strtolower($name);
        $name = str_replace(["Ą","ą"],"a",$name);
        $name = str_replace(["Č","č"],"c",$name);
        $name = str_replace(["Ę","Ė","ę","ė"],"e",$name);
        $name = str_replace(["Į","į"],"i",$name);
        $name = str_replace(["Š","š"],"s",$name);
        $name = str_replace(["Ų","Ū","ų","ū"],"u",$name);
        $name = str_replace(["Ž","ž"],"z",$name);

        $surname = strtolower($surname);
        $surname = str_replace(["Ą","ą"],"a",$surname);
        $surname = str_replace(["Č","č"],"c",$surname);
        $surname = str_replace(["Ę","Ė","ę","ė"],"e",$surname);
        $surname = str_replace(["Į","į"],"i",$surname);
        $surname = str_replace(["Š","š"],"s",$surname);
        $surname = str_replace(["Ų","Ū","ų","ū"],"u",$surname);
        $surname = str_replace(["Ž","ž"],"z",$surname);

        $scernarios = [
            $name .'@'. $surname .'.com',
            $name .'@'. $surname .'.eu',
            $surname .'@'. $name .'.com',
            $surname .'@'. $name .'.eu',
            $name .'.'. $surname .'@'. $randomServer,
            $name .'_'. $surname .'@'. $randomServer,
            $surname .'.'. $name .'@'. $randomServer,
            $surname .'_'. $name .'@'. $randomServer,
            substr($name,0,3) .'.'. substr($surname,0,3) .'@'. $randomServer,
            substr($name,0,3) .'_'. substr($surname,0,3) .'@'. $randomServer,
            substr($name,0,3) . substr($surname,0,3) .'@'. $randomServer,
            substr($surname,0,3) .'.'. substr($name,0,3) .'@'. $randomServer,
            substr($surname,0,3) .'_'. substr($name,0,3) .'@'. $randomServer,
            substr($surname,0,3) . substr($name,0,3) .'@'. $randomServer
        ];

        return $scernarios[rand(0,count($scernarios)-1)];

    }
}