<?php

class IbanGenerator
{
    public $contryCodes = ['LT','LV','EE','DE','CN','UK','TL','RU','BE'];

    function iban($contryCode = null, $length = 20, $start = null) {
        if ($contryCode == null) {
            $contryCode = $this->contryCodes[rand(0,count($this->contryCodes)-1)];
        }

        $length = $length - strlen($contryCode) - ($start != null ? strlen($start) : 0);

        $iban = strtoupper($contryCode) . ($start != null ? $start : '');

        for ($i = 0;$i < $length;++$i) {
            $iban .= rand(0,9);
        }

        return $iban;
    }
}