<?php

class DbConn
{

    private $conn;

    function __construct() {

        global $db;

        if (!$this->conn = new mysqli($db['hostname'], $db['username'], $db['password'], $db['database'])) {
            die("Connection failed: " . $this->conn->connect_error);
        }

        $this->conn->query("SET NAMES utf8");
    }

    // RUN SQL
    function run($sql) {

        if (!$result = $this->conn->query($sql)) {
            die($this->conn->error ."<br>". $sql);
        }

        if (preg_match("/^select/i",$sql, $matches, PREG_OFFSET_CAPTURE)) {
            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    $return[] = $row;
                }
            } else {
                $return = null;
            }
        } else {
            return $result;
        }

        return $return;
    }
}