<?php

class NameGenerator
{
    public $clientNames = [
        ['Ieva','Ona','Gerda','Naktibalda','Nijolė','Petrė','Ramunė','Rumunė','Egzema','Gunda',
            'Gabija','Emilija','Gabrielė','Kamilė','Ugnė','Austėja','Ieva','Viktorija','Greta','Urtė',
            'Viltė','Karolina','Rugilė','Miglė','Evelina','Goda','Augustė','Deimantė','Ema','Liepa',
            'Saulė','Paulina','Aistė','Kotryna','Karina','Gustė','Agnė','Smiltė','Monika','Akvilė',
            'Lėja','Laura','Patricija','Eva','Rusnė','Marija','Samanta','Erika','Sofija','Eglė'],
        ['Jonas','Petras','Sugertas','Radijus','Bernardas','Kaušmantas','Gerdvilas','Cigaras','Kurmis','Simonas',
            'Lukas','Matas','Nojus','Dominykas','Dovydas','Mantas','Rokas','Ignas','Martynas','Tomas',
            'Jokūbas','Arnas','Kajus','Deividas','Karolis','Paulius','Justas','Danielius','Benas','Emilis',
            'Augustas','Domantas','Domas','Gabrielius','Kristupas','Gustas','Titas','Edvinas','Laurynas','Eimantas',
            'Adomas','Vilius','Armandas','Erikas','Pijus','Tadas','Nedas','Ernestas','Ugnius','Joris']
    ];
    public $clientSurnames = [
        ['Gaudvilytė','Gaudvilienė','Gražutė','Negraži','Petraitienė','Jonaitienė','Obuolienė','Kriaušytė','Žvakidė','Nerimantienė',
            'Kurmienė','Kurmytė','Šeškutė','Kirmiz','Patranka','Meškutė','Meška','Meškauskienė','Butelka','Eglė',
            'Kuzmina','Ivonienė','Štangienė','Štangytė','Keturpėdė','Johanson','Anderson','Buda','Jopapa','Perkojaperšokanti'],
        ['Gaudvilas','Gaudmusis','Gražutis','Negražus','Petraitis','Jonaitis','Obelys','Belekoks','Stalas','Nerimantis',
            'Vėdras','Ivonis','Šeškus','Kirmiz','Šovinys','Meška','Meškauskis','Alus','Beržiška','Hantelis',
            'Plūduras','Johanson','Anderson','Kirvis','Pinigas','Nesugalvojus','Perkelmąperšokąs','Stiklinis','Medinis','Guminis']
    ];

    function name($gender = null) {
        if ($gender == null) {
            $gender = mt_rand(0,1);
        }

        $firstName = $this->clientNames[$gender][rand(0,count($this->clientNames[$gender])-1)];
        $lastName = $this->clientSurnames[$gender][rand(0,count($this->clientSurnames[$gender])-1)];

        return [$firstName,$lastName];
    }
}