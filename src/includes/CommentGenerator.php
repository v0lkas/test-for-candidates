<?php

class CommentGenerator
{
    private $paymentWords = ['payment','cash','amount','transfer','thank you','bill','account','invoice','proforma',
        'money','for me','fix','salary','fee','reward','gain'];

    function setNew() {
        $loops = mt_rand(1,3);
        $comment = '';
        $used = [];
        for ($i = 0; $i <= $loops; ++$i) {
            $wordArray = array_diff($this->paymentWords, $used);
            sort($wordArray);
            $wordToUse = $wordArray[mt_rand(0,count($wordArray)-1)];
            $used[] = $wordToUse;
            if (!empty($comment)) {
                $comment .= ' '. $wordToUse;
            } else {
                $comment = ucfirst($wordToUse);
            }
        }

        return $comment;
    }
}