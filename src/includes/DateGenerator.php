<?php

class DateGenerator
{
    private $lastTimestamp;

    public function randomDateTime() {
        return (new DateTime(rand(2016,2017) .'-'. rand(1,12) .'-'. rand(1,31)))->format("Y-m-d")
        .' '. rand(0,23) .':'. rand(0,59) .':'. rand(0,59);
    }

    public function randomCode($gender = null) {
        if ($gender == null) {
            $gender = rand(0,1);
        }

        $birthYear = rand(30,99);
        $birthMonth = rand(1,12);
        $birthDay = rand(1,30);
        $anyNumber = rand(100,9999);

        $code = ($gender == 1 ? 3 : 4) .
            $birthYear .
            ($birthMonth < 10 ? '0' : '') . $birthMonth .
            ($birthDay < 10 ? '0' : '') . ($birthMonth == 2  && $birthDay > 28 ? '28' : $birthDay) .
            ($anyNumber < 1000 ? '0' : '') . $anyNumber;

        return $code;
    }

    public function rangedTimestamp($startTime,$endTime,$transactionsCount,$currentTransaction) {
        if (empty($this->lastTimestamp)) {
            $lastTimestamp = $startTime;
        } else {
            $lastTimestamp = $this->lastTimestamp;
        }

        $section = number_format(($endTime - $startTime) / ($transactionsCount),0,"","");

        $start = $lastTimestamp;
        $end = $startTime + ($currentTransaction * $section) -1;

        $randTime = mt_rand($start,$end);

        $this->lastTimestamp = $randTime;

        return (new DateTime())->setTimestamp($randTime)->format("Y-m-d H:i:s");
    }
}