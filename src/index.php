<?php
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);
mb_internal_encoding("UTF-8");

require '/var/www/config.php';

// INCLUDE ALL METHODS
if($handle = opendir($baseDir .'/includes')) {
    while (false !== ($entry = readdir($handle))) {
        if(filetype($baseDir .'/includes/'.$entry) == 'file' && substr($entry,-4) == '.php') {
            require_once $baseDir .'/includes/'.$entry;
        }
    }
}

// READ URL
$url = explode("/",$_SERVER['REQUEST_URI']);

// READ $_GET DATA
if (isset($url[1]) && !empty($url[1])) {
    $path = $url[1];

    if (isset($url[2]) && !empty($url[2])) {
        $func = $url[2];

        if (isset($url[3]) && !empty($url[3])) {
            for ($i = 3; isset($url[$i]) && !empty($url[$i]) && $i < 22; ++$i) {
                $data[] = $url[$i];
            }
        }
    }
}

// REQUIRE SCRIPT FILE
require_once $baseDir ."/paths/index.php";
if (isset($path) && !empty($path) && file_exists($baseDir ."/paths/$path.php")) {
    require_once $baseDir ."/paths/$path.php";
} else {
    $func = (!isset($path) || empty(($path)) || $path == 'index' ? 'index' : 'error');
    $path = 'index';
}

// LAUNCH SCRIPT
if (method_exists($path,isset($func) ? $func : $path)) {
    $function = isset($func) ? $func : $path;
    $run = (new $path(isset($data) ? $data : null))->$function();
} else {
    $run = (new index())->error();
}