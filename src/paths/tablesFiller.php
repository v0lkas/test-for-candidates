<?php

class tablesFiller
{
    public $data;
    public $db;


    public function __construct($data = []) {
        $this->data = $data;
        $this->db = new DbConn();
    }


    function tablesFiller() {
        echo '<ul>
    <li><a href="/tablesFiller/clients">Generate clients table</a></li>
    <li><a href="/tablesFiller/ibans">Generate ibans table</a></li>
    <li><a href="/tablesFiller/transactions">Generate transactions & balances tables</a></li>
</ul>';
    }

    // FILL TABLE OF CLIENTS
    public function clients() {

        $usedMail = [];

        if (isset($this->data[0]) && !empty($this->data[0])) {
            $numberOfClients = $this->data[0];
        } else {
            $numberOfClients = 500;
        }

        for ($i = 0; $i < $numberOfClients; ++$i) {

            $gender = mt_rand(0,1);

            list($firstName,$lastName) = (new NameGenerator())->name($gender);
            $client[$i]['name'] = $firstName ." ".$lastName;

            do {
                $client[$i]['username'] = (new MailGenerator())->mail($firstName, $lastName);

                if (!isset($usedMail[$client[$i]['username']])) {
                    $usedMail[$client[$i]['username']] = true;
                    $mailIsUsed = false;
                } else {
                    $mailIsUsed = true;
                }
                $checkForDublicatesSql = "SELECT id FROM clients WHERE username = '". $client[$i]['username'] ."' LIMIT 1;";
                $resultDub = $this->db->run($checkForDublicatesSql);
            } while (isset($resultDub[0]['id']) || $mailIsUsed != false);

            $client[$i]['code'] = (new DateGenerator())->randomCode($gender);

            $client[$i]['active'] = (mt_rand(1,10) < 10 ? '1' : '0');
            $client[$i]['approved'] = (mt_rand(1,10) < 10 ? '1' : '0');
            $client[$i]['blocked'] = (mt_rand(1,10) < 10 ? '0' : '1');
            $client[$i]['suspended'] = (mt_rand(1,10) < 10 ? '0' : '1');

            $client[$i]['date'] = (new DateGenerator())->randomDateTime();
        }

        if (count($client)) {
            $values = '';
            $names = '';
            foreach ($client as $cl) {
                if (!empty($values)) {
                    $values .= ",\n";
                }
                $values .= '(';
                foreach ($cl as $i => $clValue) {
                    // Create list of table names on first foreach
                    if ($cl['name'] == $client[1]['name'] && $cl['code'] == $client[1]['code']) {
                        if (!empty($names)) {
                            $names .= ',';
                        }
                        $names .= $i;
                    }
                    // Create list of values
                    if (substr($values, -1) != '(') {
                        $values .= ',';
                    }
                    $values .= '\'' . $clValue . '\'';
                }
                $values .= ')';
            }
            $insertSql = "INSERT INTO clients ($names) VALUES $values";

            $this->db->run($insertSql);
            echo count($client) .' clients created.<br>';
        }
    }

    // FILL TABLE OF IBANS
    public function ibans() {

        $usersWithNoIbanSql = "SELECT
            c.id
          FROM clients c
            LEFT JOIN ibans i ON i.user_id = c.id
          WHERE i.iban IS NULL;";
        $resultUsr = $this->db->run($usersWithNoIbanSql);

        if (isset($resultUsr) && count($resultUsr)) {
            foreach ($resultUsr as $cl) {
                $ibanCount = (mt_rand(0,2) != 0 ? 1 : mt_rand(2,3));

                for ($j = 0; $j < $ibanCount; ++$j) {

                    if (isset($iban)) {
                        $i = count($iban);
                    } else {
                        $i = 0;
                    }

                    $iban[$i]['user_id'] = $cl['id'];

                    do {
                        $iban[$i]['iban'] = (new IbanGenerator())->iban('LT', 20, 77700);

                        if (!isset($usedIban[$iban[$i]['iban']])) {
                            $usedIban[$iban[$i]['iban']] = true;
                            $ibanIsUsed = false;
                        } else {
                            $ibanIsUsed = true;
                        }
                        $checkForDublicatesSql = "SELECT user_id FROM ibans WHERE iban = '" . $iban[$i]['iban'] . "' LIMIT 1;";
                        $resultDub = $this->db->run($checkForDublicatesSql);
                    } while (isset($resultDub[0]['user_id']) || $ibanIsUsed != false);
                }
            }

            if (count($iban)) {
                $values = '';
                $names = '';
                foreach ($iban as $cl) {
                    if (!empty($values)) {
                        $values .= ",\n";
                    }
                    $values .= '(';
                    foreach ($cl as $i => $clValue) {
                        // Create list of table names on first foreach
                        if ($cl['iban'] == $iban[1]['iban'] && $cl['user_id'] == $iban[1]['user_id']) {
                            if (!empty($names)) {
                                $names .= ',';
                            }
                            $names .= $i;
                        }
                        // Create list of values
                        if (substr($values, -1) != '(') {
                            $values .= ',';
                        }
                        $values .= '\'' . $clValue . '\'';
                    }
                    $values .= ')';
                }
                $insertSql = "INSERT INTO ibans ($names) VALUES $values";

                $this->db->run($insertSql);
                echo count($iban) .' ibans created.<br>';
            }
        } else {
            echo 'The is no users without iban.';
            exit;
        }
    }

    // FILL TABLES OF TRANSACTIONS & BALANCES
    public function transactions() {
        $datesSelectorSql = "SELECT DATE(MIN(`date`)) AS start_date, DATE(MAX(`date`)) AS end_date FROM clients;";
        $resultDates = $this->db->run($datesSelectorSql);

        $startDate = new DateTime($resultDates[0]['start_date']);
        $endData = (new DateTime($resultDates[0]['end_date']))->modify('+1 day');

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startDate, $interval, $endData);

        $totalCreated = 0;  // $total TRNS counter. Will stop after 10000 will be reached
        $maxOneSessionCreate = 25000;

        // Looping dates
        foreach ($period as $dt) {
            $currentDate = $dt->format("Y-m-d");

            // Check if there any any transactions already created on that day
            $checkForTrnsSql = "SELECT id FROM transactions WHERE DATE(created) = '$currentDate' LIMIT 1;";
            $resultTrnsOnDate = $this->db->run($checkForTrnsSql);

            unset($balance);
            unset($trn);

            if (!isset($resultTrnsOnDate) || $resultTrnsOnDate == 0) {
                // Make list of clients who were registered on that day
                $clientsSelectorSql = "SELECT
                    c.id AS user_id,
                    c.`name`,
                    IFNULL((SELECT balance FROM balances WHERE iban = i.iban AND `date` < '".$currentDate."' ORDER BY `date` DESC LIMIT 1),'0') AS balance,
                    i.iban
                FROM clients c
                LEFT JOIN ibans i ON i.user_id = c.id
                WHERE DATE(`date`) <= '$currentDate';";
                $resultClients = $this->db->run($clientsSelectorSql);

                if (isset($resultClients[0])) {
                    $transactionsCount = mt_rand(1,count($resultClients)); // how much transactions should be created

                    $startTime = (new DateTime($currentDate. ' 00:00:00'))->getTimestamp();
                    $endTime = (new DateTime($currentDate. ' 23:59:59'))->getTimestamp();

                    $dateGenerator = new DateGenerator();

                    // Loop transaction creation
                    for ($i = 0; $i < $transactionsCount; ++$i) {
                        // Choose any client who was active on that date
                        $userRand = mt_rand(0,count($resultClients)-1);

                        // Set our client data
                        $trn[$i]['user_id'] = $resultClients[$userRand]['user_id'];
                        $trn[$i]['iban'] = $resultClients[$userRand]['iban'];

                        // Get actual user balance
                        $currentBalance = (isset($balance[$trn[$i]['iban']]) ? $balance[$trn[$i]['iban']] : $resultClients[$userRand]['balance']);

                        // Set transaction type
                        $types = ['incoming','outgoing'];
                        $trn[$i]['type'] = ($currentBalance >= 0.01 ? $types[mt_rand(0,1)] : $types[0]);

                        // Set transaction amount
                        $trn[$i]['amount'] = (new Numbers())->roundToHundreds(0.01, ($trn[$i]['type'] == $types[1] ? $currentBalance : mt_rand(500,10000)));

                        // Fill FROM:TO data
                        if ($trn[$i]['type'] == $types[0]) {    // incoming
                            $side1 = 'to';
                            $side2 = 'from';
                        } else {                                // outgoing
                            $side1 = 'from';
                            $side2 = 'to';
                        }
                        $trn[$i][$side1.'_iban'] = $resultClients[$userRand]['iban'];
                        $trn[$i][$side1.'_name'] = $resultClients[$userRand]['name'];
                        $trn[$i][$side2.'_iban'] = (new IbanGenerator())->iban(null,mt_rand(20,30));
                        $newName = (new NameGenerator())->name();
                        $trn[$i][$side2.'_name'] = $newName[0] .' '. $newName[1];

                        // Comment generator
                        $trn[$i]['comment'] = (new CommentGenerator())->setNew();

                        // Creation date
                        $trn[$i]['created'] = $dateGenerator->rangedTimestamp($startTime,$endTime,$transactionsCount,$i + 1);

                        $balance[$trn[$i]['iban']] = number_format($currentBalance + ($trn[$i]['amount'] * ($trn[$i]['type'] == $types[0] ? 1 : -1)),2,".","");
                    }

                    // Write to DB
                    if (count($trn)) {

                        // Create TRNs
                        foreach ($trn as $cl) {
                            $values = '';
                            $names = '';

                            if (!empty($values)) {
                                $values .= ",\n";
                            }
                            $values .= '(';
                            foreach ($cl as $i => $clValue) {
                                // Create list of table names on first foreach
                                if (!empty($names)) {
                                    $names .= ',';
                                }
                                $names .= $i;
                                // Create list of values
                                if (substr($values, -1) != '(') {
                                    $values .= ',';
                                }
                                $values .= '\'' . $clValue . '\'';
                            }
                            $values .= ')';

                            $insertSql = "INSERT INTO transactions ($names) VALUES $values;";
                            $this->db->run($insertSql);
                        }

                        // Create balances
                        $values = '';
                        foreach ($balance as $bIban => $bBalance) {
                            if (!empty($values)) {
                                $values .= ",\n";
                            }
                            $values .= "('$currentDate','$bIban','$bBalance')";
                        }
                        $insertSql = "INSERT INTO balances (date,iban,balance) VALUES $values;";

                        $this->db->run($insertSql);

                        // Echo result
                        echo '<strong>'.$currentDate.'</strong>: '. count($trn) .' transactions created ; '. count($balance) .' balances updates;<br>';

                        $totalCreated += count($trn);
                    }
                }
            }

            if ($totalCreated >= $maxOneSessionCreate) {
                break;
            }
        }
    }
}