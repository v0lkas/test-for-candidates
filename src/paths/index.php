<?php

class index
{
    public $data;

    public function __construct($data = []) {
        $this->data = $data;
    }

    public function index() {
        echo 'This is start page.';
    }

    public function error() {
        echo 'Error 404! File not found.';
    }
}